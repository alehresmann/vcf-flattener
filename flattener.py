# Anne-Laure Ehresmann
# alehresmann@gmail.com

import os.path
import sys
import vcf
import warnings

# known info tags
ignore_info = [
    "BaseQRankSum", "ClippingRankSum", "ExcessHet", "FS", "MQ", "MQRankSum",
    "QD", "ReadPosRankSum", "SOR", "RPA", "RU", "STR", "DP", "AN", "END", "NS",
    "RAW_MQ", "DB"
]
to_fix_info = ["AC", "AF", "MLEAC", "MLEAF", "CIGAR"]

# known format tags
ignore_format = [
    "DP", "FT", "HQ", "PS", "PQ", "MQ", "GQ", "MIN_DP", "SB", "DP", "GL", "PID"
]
# format keys that are updated by this script
special_format = ["GT", "PL", "SAC", "AD", "PGT"]


def get_file():
    if len(sys.argv) <= 1:
        warnings.warn(
            "You must give me an input file! (optionally, an output file name"
            " too)")
        sys.exit(0)

    filepath = sys.argv[1]
    if len(sys.argv) < 3:
        from time import gmtime, strftime
        outputpath = filepath + "_flattened_" + \
            strftime("%Y-%m-%d_%H:%M:%S.vcf", gmtime())
    else:
        outputpath = sys.argv[2]
    if not os.path.isfile(filepath):
        warnings.warn("File does not exist")
        sys.exit(0)
    return [filepath, outputpath]


def get_loc_string(record, sample=None):
    s = str(record.CHROM) + ":" + str(record.POS)
    if sample is not None:
        s += ":" + sample.sample
    return s


def get_max_ac_index_and_alt_allele_count(record):
    if "AC" not in record.INFO.keys():
        warnings.warn(
            get_loc_string(record) +
            ": Error: AC not in INFO tag, cannot continue.")
        sys.exit(0)
    return [
        record.INFO["AC"].index(max(record.INFO["AC"])),
        len(record.INFO["AC"])
    ]


def flatten_info(k, record):
    # deals with anything that needs to be flattened within the INFO field

    keys = record.INFO.keys()
    for key in keys:
        if key not in ignore_info and key not in to_fix_info:
            ignore_info.append(key)
            warnings.warn(
                get_loc_string(record) +
                str("warning: unknown INFO tag " + key +
                    ", ignoring, but it's possible they actually need to be"
                    "changed during the flattening."))
    # these are easy: only keep the one about the minor allele
    for key in to_fix_info:
        if key in keys:
            record.INFO[key] = [record.INFO[key][k]]


def verify_format(record, formt):
    for key in formt:
        if key not in ignore_format and key not in special_format:
            ignore_format.append(key)
            warnings.warn(
                get_loc_string(record) +
                str(": warning: unknown FORMAT tag " + key +
                    ", ignoring, but it's possible they actually need to be"
                    "changed during the flattening."))
    if "GLE" in formt and "GLE" not in ignore_format:
        ignore_format.append("GLE")
        warnings.warn(
            get_loc_string(record) +
            str(": warning: As VCF 4.3, the GLE field is deprecated. As such,"
                " I have opted to ignore it."))


def _genotype_helper(gt, max_allele_index):
    # since haploids can exist, check if |, /, or neither
    break_slash_neither = -1
    if "|" in gt:
        break_slash_neither = 0
    elif "/" in gt:
        break_slash_neither = 1
    else:
        break_slash_neither = 2

    gt = gt.replace("|", "/").split("/")
    for j, g in enumerate(gt):
        # if genotype is neither ref nor missing, if it's the minor
        # allele, set it to 1, else, set it to missing.
        if g != '0' and g != '.':
            if g == str(max_allele_index):
                gt[j] = '1'
            else:
                gt[j] = "."
    # rebuild the genotype with | or /
    if break_slash_neither == 0:
        gt = "|".join(gt)
    if break_slash_neither == 1:
        gt = "/".join(gt)
    if break_slash_neither == 2:
        gt = "".join(gt)
    return gt


def _genotype_count_helper(record, sample, field, max_allele_index, t):
    # for fields that have a number of values equal to the num of possible
    # genotype
    # location of 0/0 is always the first. location of 0/1 and 1/1
    # depends on the minor allele's index
    if len(field) != sum(range(t + 1)) + t + 1:
        # note: technically, num of possible genotypes
        # is sum(t + (t-1) + ... + 1) + t + 1, but
        # because range(x) is a range from
        # 1 to x-1, then I give it the val t + 1
        warnings.warn(
            get_loc_string(record, sample) +
            str(": field PL, GL, or GP had length " + str(len(field)) +
                " when we expected length " +
                str(sum(range(max_allele_index + 1))) +
                ", keeping unchanged."))
        return field
    return [
        field[0], field[sum(range(max_allele_index + 1))],
        field[sum(range(max_allele_index + 1)) + max_allele_index]
    ]


def flatten_genotype(k, t, record):
    verify_format(record, record.FORMAT.split(":"))
    max_allele_index = k + 1  # when counting ref allele as well
    for i, sample in enumerate(record):
        data = sample.data._asdict()
        # see if you need to waste time rebuilding the record or not,
        # depending on whether I changed the sample or not
        changed = False
        if "GT" in data.keys() and data["GT"] is not None:
            changed = True
            data["GT"] = _genotype_helper(data["GT"], max_allele_index)

        if "PGT" in data.keys() and data["PGT"] is not None:
            changed = True
            data["PGT"] = _genotype_helper(data["PGT"], max_allele_index)

        if "PL" in data.keys() and data["PL"] is not None:
            changed = True
            data["PL"] = _genotype_count_helper(record, sample, data["PL"],
                                                max_allele_index, t)

        if "SAC" in data.keys() and data["SAC"] is not None:
            changed = True
            if len(data["SAC"]) < max_allele_index * 2 + 2:
                # don't have SAC info for max_allele.
                # then, keep only info about the ref allele.
                # note that python allows indexing even when the index goes
                # past the list length, so even if data["SAC"] was < 2, the
                # following line works as intended.
                data["SAC"] = data["SAC"][:2]
            else:
                # else, keep the first two entries (about the ref) and the
                # minor allele entries
                data["SAC"] = [
                    data["SAC"][0], data["SAC"][1],
                    data["SAC"][max_allele_index * 2],
                    data["SAC"][max_allele_index * 2 + 1]
                ]
        if "AD" in data.keys() and data["AD"] is not None:
            # as expected, keep ref and minor allele entries
            if (len(data["AD"]) <= max_allele_index):
                warnings.warn(
                    get_loc_string(record, sample) +
                    ": AD tag has fewer entries than there are alleles."
                    " ignoring.")
            else:
                changed = True
                data["AD"] = [data["AD"][0], data["AD"][max_allele_index]]

        if "EC" in data.keys() and data["EC"] is not None:
            # as expected, keep ref and minor allele entries
            changed = True
            data["EC"] = [data["EC"][0], data["EC"][max_allele_index]]

        if "GL" in data.keys() and data["GL"] is not None:
            # all possible genotypes order
            data["GL"] = _genotype_count_helper(record, sample, data["GL"],
                                                max_allele_index, t)
        if "GP" in data.keys() and data["GP"] is not None:
            # possible genotypes order
            data["GP"] = _genotype_count_helper(record, sample, data["GP"],
                                                max_allele_index, t)

        # finished fixing the record sample, now reinsert it in the
        # record if it was edited
        if changed:
            altered_record = vcf.model.make_calldata_tuple(sample.data._fields)
            keys = [data[k] for k in altered_record._fields]
            sample.data = altered_record._make(keys)
            record.samples[record._sample_indexes[sample.sample]] = sample


def main():
    inpt, outpt = get_file()
    vcf_reader = vcf.Reader(open(inpt, 'r'))
    vcf_writer = vcf.Writer(open(outpt, 'w'), vcf_reader)
    for record in vcf_reader:
        # if site is polyallelic, it needs to be flattened:
        if len(record.INFO["AC"]) > 1:
            k, t = get_max_ac_index_and_alt_allele_count(record)
            flatten_info(k, record)
            flatten_genotype(k, t, record)
            # make the ALT field only hold the minor allele
            record.ALT = [record.ALT[k]]
            vcf_writer.write_record(record)
        else:
            vcf_writer.write_record(record)


main()
