# VCF-flattener
A short python script for making a polyallelic VCF (and GVCF) biallelic. It was written for the **VCF version 4.3**.

It can be used to make a polyallelic (also called multiallelic) VCF file into a diallelic (also called biallelic) VCF, and fixes all the additional fields related to biallelic data. I recommend using this over vcflib's vcf-flatten script, as their don't update FORMAT fields properly.

## What can this be used for?
A number of VCF tools and libraries do not support polyallelic sites. A simple solution to this is to filter out all polyallelic sites. But what if you think there may be useful information in the polyallelic sites, which you do not want to filter out ? This may happen for instance, if your set of individuals is numerous and/or especially varied, which would heighten the chances of you spotting multiple alternate alleles at a specific site, or, perhaps your study specifically focuses on rare alleles and you think that removing polyallelic sites might bias your dataset.

## Simply put, what does this do (TL;DR)?
This script looks at all your sites, spots the ones which have more than one alternate allele, looks at the allele frequencies for each, and only keeps the reference allele and the one which has the **second-highest allele frequency** (hereby referred to the MAF, minor allele frequency), after the reference allele frequency. Any other allele, it merely makes as "unknown" (with a single dot (.) .), thereby removing any issues any libraries or tools may have with polyallelic allelic sites. The script then goes and checks all additional data on the site, and updates it if required. If it encounters a tag it is unfamiliar with, it reports it to the user and leaves it untouched.

## How can I use it?
Required dependencies: [PyVCF](https://pyvcf.readthedocs.io/en/latest/), which you can install with pip with `pip install --user pyvcf`
This was written in python 3.7

## With high detail, what does this do?
INFO fields(if present) that are updated by this script:
* AC (allele count): Only minor allele is kept. (e.g. `AC=0,1,5;` becomes `AC=5;`). The original index (hereby called **k**, in this document starting at 1, but in python, at 0) of the minor allele is then remembered for other fields, the number of total alternate alleles previously present is also remembered temporarily (hereby called **t**)
* AF (allele frequency): Only the value at index *k* is kept. (e.g. `AF=0,0.0001,0.0034;` becomes `AC=0.0034;`)
* MLEAC (Maximum likelihood expectation for the allele counts): Only the value at index *k* is kept.
* MLEAF (Maximum likelihood expectation for the allele frequencies): Only the value at index *k* is kept.
* MLEAF (Maximum likelihood expectation for the allele frequencies): Only the value at index *k* is kept.
* CIGAR (CIGAR string): Only the value at index *k* is kept.
* **RPA**: I have yet to find good documentation for how RPA exactly works. As such, I currently ignore it, but I believe it should technically be edited by the script. Any clarification is welcome.
* **GLE**: being deprecated as of VCF 4.3, I have opted to ignore it, however I can support it if required by some users.
* **PID**: I *believe* PID is always written as `chrompos_refallele_altallele[_altallele]`, but given that I have not yet found a clear confirmation of this, I do not touch this entry for now. Unfortunately, none of my own VCFs use this. Clarification is welcome.

For each sample where the any of the above INFO fields were changed, the following FORMAT entries are updated:
 * GT (genotype): The genotype is updated to represent the correct genotype according to the AC change. Since the minor allele becomes the 2nd allele, it will be changed to a 1, and any other non reference allele will be changed to a . (e.g. if *k = 2*, then the genotype `0/2` is updated to `0/1`, and genotypes showing any other alternate alleles (eg `1/1`, `0/3`, ...) lose their alternate alleles (changed to `./.`, `0/.`, ...))
 * PL (Phred-scaled genotype): PL is updated to keep only information about the ref allele and the allele with the max frequency.
 * SAC (Strand Allele Counts): SAC is updated to keep only information about the ref allele and the allele with the max frequency, if it is available. Otherwise, only the information about the reference allele is kept. (length is checked first. e.g. with *k = 1*,  `SAC = 143,378,4202,0,3824,3797` becomes `143,378,4202,0`)

## Can I get an example of the output for this ?
I've provided an example test.vcf, and it's output, test\_flattened.vcf, in the files. I've tried to anonymise it as best I could, let me know if it is improperly anonymised somehow. You'll notice that if you run the script on test.vcf, you'll get a bunch of warnings about the AD field. This was an issue with the vcf file I used, I figured I'd keep it in to show examples of how possible errors are reported (i.e. \[chrom\] \[pos\] \[sample\]: \[error\]).

## Help ! \[some field\] is ignored by this script, when it really should be doing \[x y z\]!
Send me an email and I will fix it.
